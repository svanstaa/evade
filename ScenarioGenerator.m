function [xEgo,yEgo,xObj,yObj] = ScenarioGenerator(vEgo,vObj,GeometryEgo,GeometryObj,timeStep)
%   Generates trajectories for 90� collisions
%
%   vEgo        = velocity of ego vehicle (m/s)
%   timeStep    = time diff between data points (seconds) 
%   timeCodes   = vector of time stamps
%   xEgo        = vector of Ego x locations
%   GeometryEgo = [length,width] of ego vehicle
%   GeometryObj = [length,width] of target object

timeCodes = -3:timeStep:0;
xEgo = timeCodes * vEgo - (GeometryEgo(1));
yEgo = timeCodes * 0    - (GeometryEgo(2)/2);
xObj = timeCodes * 0    - (GeometryObj(2)/2);
yObj = timeCodes * vObj - (GeometryObj(1));

ax_max = 10;
ttb = TTB_Calc(xEgo,yEgo,ax_max);

Visualize(xEgo,yEgo,xObj,yObj);

end

