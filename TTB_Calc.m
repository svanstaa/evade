function [ttb] = TTB_Calc(xEgo,yEgo,ax_max)
% Calculates TimeToBrake. Assumes constant ego velocity, constant braking
% ax=10m/s^2

v = yEgo(2)-yEgo(1);
ttb = v / ax_max;
end
