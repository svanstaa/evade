function [] = Visualize(xEgo,yEgo,xObj,yObj)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

clf('reset')
axis([-25, 25, -25,25]);

Ego = rectangle('EdgeColor','k','FaceColor','b');
Obj = rectangle('EdgeColor','k','FaceColor','r');

for i = 1:size(xEgo,2)
  
  Ego.Position = ([yEgo(i),xEgo(i),2,5]);
  Obj.Position = ([yObj(i),xObj(i),5,2]);
  drawnow
  pause(0.1)
  
end

end

